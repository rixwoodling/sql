create table products (
	id INT,
	isbn VARCHAR(50),
	product VARCHAR(50),
	country VARCHAR(50)
);
insert into products (id, isbn, product, country) values (1, '534648095-1', 'Petite Baguette', 'Greece');
insert into products (id, isbn, product, country) values (2, '809752354-X', 'Longos - Lasagna Veg', 'Philippines');
insert into products (id, isbn, product, country) values (3, '360566652-3', 'Leeks - Large', 'Ireland');
insert into products (id, isbn, product, country) values (4, '841194403-4', 'Flower - Leather Leaf Fern', 'Thailand');
insert into products (id, isbn, product, country) values (5, '531459542-7', 'Beer - Camerons Cream Ale', 'China');
insert into products (id, isbn, product, country) values (6, '797637260-2', 'Soup Bowl Clear 8oz92008', 'Croatia');
insert into products (id, isbn, product, country) values (7, '595859962-3', 'Oneshot Automatic Soap System', 'Belarus');
insert into products (id, isbn, product, country) values (8, '439768569-X', 'Pants Custom Dry Clean', 'China');
insert into products (id, isbn, product, country) values (9, '445827896-7', 'Artichoke - Bottom, Canned', 'Indonesia');
insert into products (id, isbn, product, country) values (10, '498253004-1', 'Soup - Knorr, Chicken Noodle', 'Indonesia');