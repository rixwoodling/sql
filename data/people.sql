create table people (
	id INT,
	firstname VARCHAR(50),
	lastname VARCHAR(50),
	email VARCHAR(50),
	gender VARCHAR(50),
	country VARCHAR(50)
);
insert into people (id, firstname, lastname, email, gender, country) values (1, 'Euell', 'Cominetti', 'ecominetti0@ftc.gov', 'Male', 'Argentina');
insert into people (id, firstname, lastname, email, gender, country) values (2, 'Delly', 'Zamboniari', 'dzamboniari1@dailymail.co.uk', 'Female', 'Spain');
insert into people (id, firstname, lastname, email, gender, country) values (3, 'Yankee', 'Voak', 'yvoak2@people.com.cn', 'Male', 'China');
insert into people (id, firstname, lastname, email, gender, country) values (4, 'Arturo', 'Swayton', 'aswayton3@godaddy.com', 'Male', 'Thailand');
insert into people (id, firstname, lastname, email, gender, country) values (5, 'Sergeant', 'Ommanney', 'sommanney4@pbs.org', 'Male', 'China');
insert into people (id, firstname, lastname, email, gender, country) values (6, 'Norman', 'Bautiste', 'nbautiste5@mtv.com', 'Male', 'China');
insert into people (id, firstname, lastname, email, gender, country) values (7, 'Lenci', 'Skells', 'lskells6@google.fr', 'Male', 'Angola');
insert into people (id, firstname, lastname, email, gender, country) values (8, 'Prentiss', 'Tiffney', 'ptiffney7@eepurl.com', 'Male', 'Indonesia');
insert into people (id, firstname, lastname, email, gender, country) values (9, 'Emmie', 'Peete', 'epeete8@mail.ru', 'Female', 'Indonesia');
insert into people (id, firstname, lastname, email, gender, country) values (10, 'Aloysia', 'Blaasch', 'ablaasch9@photobucket.com', 'Female', 'Philippines');