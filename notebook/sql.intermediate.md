##### Import SQL files to Database

show databases;
create database user;
use user;

<sup>`>` `source people.sql; /* full path */`</sup>  
<sup>`>` `source products.sql; /* full path */`</sup>  

show columns from people;
show columns from products;

| <sup>First Header</sup>  | <sup>Second Header</sup> |
| ------------------------ | ------------------------ |
| <sup>Content Cell</sup>  | <sup>Content Cell</sup>  |
| <sup>Content Cell</sup>  | <sup>Content Cell</sup>  |

##### SQL Inner Join

<sup><i>inner join = matching data from different tables</i></sup>
<sup><i>boolean operation = A and B</i></sup>  

<sup>`>` `select * from people`</sup>     
<sup>`->` `inner join products`</sup>  
<sup>`->` `on people.country = products.country;`</sup>  

<sup>`>` `select * from people`</sup>  
<sup>`->` `inner join products`</sup>  
<sup>`->` `on people.country = products.country`</sup>  
<sup>`->` `where gender='Female';`</sup>  

##### SQL Left Join

<sup><i>left join = lists all of column from first table and filter's second table</i></sup>  
<sup><i>boolean operation = A not B + A and B</i></sup>  

<sup>`>` `select * from people`</sup>  
<sup>`->` `left join products`</sup>  
<sup>`->` `on people.country = products.country;`</sup>  

##### SQL Right Join

<sup><i>left join = lists all of column from second table and filter's first table</i></sup>  
<sup><i>boolean operation = B not A + A and B</i></sup>  

<sup>`>` `select * from people`</sup>  
<sup>`->` `right join products`</sup>  
<sup>`->` `on people.country = products.country;`</sup>    
