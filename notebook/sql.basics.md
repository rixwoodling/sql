###### types of databases management systems    
<sup>`>` hiearchical dbms > macOS/linux filesystem (parent/child organization)</sup>  
<sup>`>` network dbms > rms?</sup>  
<sup>`>` relational dbms > MySQL, oracle, MS SQL Server (table format)</sup>  
<sup>`>` object oriented relation dbms > PostgreSQL</sup>  

###### about databases  
<sup>`>` simple databases > flat file databases</sup>  
<sup>`>` fields (columns) > one type of data only</sup>  
<sup>`>` text, number, date, time, graphics, calculated</sup>  
<sup>`>` records (rows)</sup>   
<sup>`>` databases can be searched and sorted</sup>  

<sup>SQL: Standard Query Language</sup>

<sup>`>` example of sql statement:</sup>  
<sup>`select * from members where age > 30`</sup>  

##### Creating a Database and Adding Tables 

<sup>`>` `create database haunted; /* create new database */`</sup>  
<sup>`>` `use haunted; /* choose a database */`</sup>  
<sup>`>` `show tables;`</sup>  
<sup>`>` `create table monsters`</sup>   
<sup>`->` `(id int(11) unsigned auto_increment primary key not null,`</sup>   
<sup>`->` `username varchar(25) not null,`</sup>   
<sup>`->` `passwd varchar(25) not null,`</sup>   
<sup>`->` `email varchar(40) not null)`</sup>  
<sup>`->` `status varchar(10) not null);`</sup>  
<sup>`>` `show tables;`</sup>  
<sup>`>` `describe user;`</sup>  

<sup>`>` `exit;`</sup>  
<sup>`>` `mysql.server restart /* macos */`</sup>  
<sup>`>` `mysql -u root -p -h localhost`</sup>  

##### Adding Data to Tables in a Database

<sup>`>` `show databases;`</sup>  
<sup>`>` `use haunted;`</sup>  
<sup>`>` `show tables;`</sup>  
<sup>`>` `describe monsters;`</sup>  

<sup>`>` `insert into monsters (username, passwd, email)`</sup>   
<sup>`->` `values ('witch','broom','witch@email.com);`</sup>  
<sup>`>` `select * from monsters;`</sup>  

<sup>`>` `insert into monsters (username, passwd, email)`</sup>  
<sup>`->` `values ('ghost','boo','ghost@email.com');`</sup>  
<sup>`>` `select * from monsters;`</sup>  

<sup>`>` `insert into monsters (username, passwd, email)`</sup>  
<sup>`->` `values ('john','sandwitch','johnnyboy@email.com');`</sup>  
<sup>`>` `select * from monsters;`</sup>  

<sup>`>` `insert into monsters (username, passwd, email)`</sup>  
<sup>`->` `values ('vampire','blood','vampire@email.com');`</sup>  
<sup>`>` `select * from monsters;`</sup>  

##### Selecting Data from Tables in a Database

<sup>`>` `show databases;`</sup>  
<sup>`>` `use haunted;`</sup>  
<sup>`>` `show tables;`</sup>  
<sup>`>` `select * from monsters;`</sup>  
<sup>`>` `select username from monsters;`</sup>  
<sup>`>` `select * from monsters where id=1;`</sup>  
<sup>`>` `select * from monsters where passwd='admin';`</sup>  
<sup>`>` `select * from monsters order by id DESC;`</sup>  

##### Updating Data in a Database

<sup>`>` `use haunted;`</sup>  

<sup>`>` `update monsters`</sup>  
<sup>`->` `set username='zombie'`</sup>    
<sup>`->` `where id=3;`</sup>  
<sup>`>` `select * from monsters;`</sup>    

<sup>`>` `update monsters`</sup>  
<sup>`->` `set passwd='brains'`</sup>  
<sup>`->` `where username='zombie';`</sup>  
<sup>`>` `select * from monsters;`</sup>  

<sup>`>` `update monsters`</sup>  
<sup>`->` `set email='zombie@email.com'`</sup>    
<sup>`->` `where passwd='brains';`</sup>  
<sup>`>` `select * from monsters;`</sup>  

<sup>`>` `update monsters`</sup>  
<sup>`->` `set status='dead';`</sup>  
<sup>`>` `select * from user;`</sup>  

##### Deleting Data from a Database

<sup>`>` `delete from user where username='johndoe';`</sup>  
<sup>`>` `delete from user where username`</sup>   
<sup>`->` `in ('zombie','monster');`</sup>  
