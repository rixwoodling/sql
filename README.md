#### SQL Setup

##### Install MySQL with homebrew macOS
<sup>`>` `brew install MySQL` Install MySQL.</sup>  
<sup>`?` `mysql_secure_installation` Alternative install.</sup>  
<sup>`>` `mysql -V` Verify install mysql.  <i>Ver 14.14 Distrib 5.7.19, for osx10.12 (x86_64)</i></sup>  
<sup>`>` `brew services list` Active services.</sup>  
<sup>`>` `ps ax | grep mysql`  Find MySQL paths.</sup>  
<sup>`>` `cd /usr/local/opt/mysql` MySQL folder.</sup>  

<sup>`+` `mysqladmin -u root password 'xxxx'` Create admin password for MySQL.</sup>  
###### Change root admin password on macOS
<sup>`+` `brew update && brew doctor && brew install mysql`</sup>  
<sup>`+` `mysql.server restart` restart mysql service</sup>  
<sup>`+` `mysql_secure_installation` set new root password</sup>  
><sup>Would you like to setup VALIDATE PASSWORD plugin? `y` password validator</sup>  
><sup>Change the password for root ? `y`</sup>  
><sup>Remove anonymous users? `y`</sup>  
><sup>Disallow root login remotely? `y`</sup>  
><sup>Remove test database and access to it? `y`</sup>  
><sup>Reload privilege tables now? `y` ensure all changes are made</sup>  

##### Enter MySQL prompt
<sup>`+` `mysql -u [username] -p` [username] is root or other.</sup>  
<sup>`+` Enter password:</sup>  

---

###### MySQL commands

<sup>`show databases;` show all databases</sup>  
<sup>`create database userlogin;` create database names userlogin</sup>  
<sup>`show tables;` show, if any, tables listed</sup>
<

MySQL
SQL tools : command line

localhost/phpmyadmin

##### Create Database
<sup>`CREATE DATABASE test;` create a database.</sup>  
<sup>`DROP DATABASE test;` delete a database.</sup>    

##### Create Table
```
CREATE TABLE customers(  
  id INT NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(255),
  lastName VARCHAR(255),
  email VARCHAR(255),
  address VARCHAR(255),
  city VARCHAR(255),
  state VARCHAR(255),
  zipcode VARCHAR(255),
  PRIMARY KEY(id)
);
```
##### Insert Data to Table
```
INSERT INTO customers (firstName, lastName, email, address, city, state, zipcode) VALUES ('John','Doe','jdoe@email.com','55 Main St','Boston','Massachussets','01221');
```

<sup>`?` https://stackoverflow.com/questions/9695362/macosx-homebrew-mysql-root-password</sup>  
<sup>`?` https://dev.mysql.com/doc/refman/5.7/en/default-privileges.html</sup>  
<sup>`?` https://stackoverflow.com/questions/36463966/when-is-flush-privileges-in-mysql-really-needed</sup>  
<sup>`?` https://gist.github.com/hofmannsven/9164408</sup>  
